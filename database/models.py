from django.db import models


# Create your models here.


class Images(models.Model):
    serial = models.IntegerField()
    tag = models.TextField()
    file = models.TextField()
    width = models.IntegerField()
    height = models.IntegerField()
    refer = models.TextField(blank=True, null=True)
    tags = models.TextField()
    favor = models.IntegerField()
    rating = models.TextField()

    class Meta:
        managed = True
        db_table = 'images'


class Tags(models.Model):
    tag = models.TextField()
    count = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'tags'

