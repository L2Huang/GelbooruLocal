# -*- coding: utf-8 -*-

from django.http import HttpResponse

from database.models import Images, Tags


def getTagList(img_id=None):
    tag_list = []
    if img_id == None:
        tags = Tags.objects.order_by('-count')[0:25]
        for tag in tags:
            tag_list.append({'tag': tag.tag, 'count': tag.count})
    else:
        result = Images.objects.filter(id=img_id)
        tags = result[0].tags
        tag_names = tags.split(" ")
        for tag_name in tag_names:
            result = Tags.objects.filter(tag=tag_name)
            tag_count = result[0].count
            tag_list.append({'tag': tag_name, 'count': tag_count})
    return tag_list


def getImageList(page, tag=None):
    if tag == None:
        results = Images.objects.filter(rating='safe').order_by('-id')
    else:
        results = Images.objects.filter(rating='safe').filter(tags__contains=tag).order_by('-id')
    return results


def getImageInfo(img_id):
    results = Images.objects.filter(id=img_id)
    result = results[0]
    img_info = {'rating': result.rating, 'file': result.file, 'tags': result.tags, 'width': result.width,
                'height': result.height, 'refer': result.refer}
    return img_info
