from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render
from database.models import Images
from . import db
from django.views.decorators.csrf import csrf_exempt


def image(request, id):
    request.encoding = 'utf-8'
    obj = 1
    img_id = id
    context = {}
    tags = db.getTagList(img_id)
    tag_list = []
    for tag in tags:
        tag_list.append({'tag': tag['tag'].replace(
            '_', ' '), 'count': tag['count'], 'link': tag['tag']})
    context['tag_list'] = tag_list
    img_info = db.getImageInfo(img_id)
    if not img_info['rating'] == 'safe':
        if not obj:
            context['message'] = 'You Should Login to View This Image!'
        else:
            context['message'] = ""
    else:
        context['message'] = ""
    if obj:
        context['loginstatus'] = "login"
    else:
        context['loginstatus'] = "logout"
    context['rating'] = img_info['rating']
    context['file'] = img_info['file']
    context['tags'] = img_info['tags']
    context['width'] = img_info['width']
    context['height'] = img_info['height']
    context['refer'] = img_info['refer']
    context['tag'] = img_info['tag']
    return render(request, 'image.html', context)


def index(request):
    request.encoding = 'utf-8'
    return HttpResponseRedirect('/index/safe/wallpaper/hd/1')


@csrf_exempt
def list(request, rate, tag, size, page):
    request.encoding = 'utf-8'
    obj = 1
    if request.method == 'POST':
        if 'search' in request.POST:
            search_tag = request.POST.get('search')
            return HttpResponseRedirect('/index/safe/' + search_tag + '/hd/1')
        if 'img_id' in request.POST:
            mark_img_id = request.POST.get('img_id')
            mark_img_mark = request.POST.get('img_mark')
            cur_favor = Images.objects.filter(id=int(mark_img_id)).first().favor
            if mark_img_mark == "like":
                Images.objects.filter(id=int(mark_img_id)).update(favor=cur_favor+1)
            if mark_img_mark == "dislike":
                Images.objects.filter(id=int(mark_img_id)).update(favor=cur_favor-1)
            return HttpResponseRedirect(request.path)
    context = {}
    if not rate == 'safe':
        if not obj:
            context['message'] = 'You Should Login to View Images Rating With Questionable or Explicit!'
        else:
            context['message'] = ""
    else:
        context['message'] = ""
    if obj:
        context['loginstatus'] = "login"
    else:
        context['loginstatus'] = "logout"
    tags = db.getTagList()
    tag_list = []
    for tag_array in tags:
        tag_list.append({'tag': tag_array['tag'].replace(
            '_', ' '), 'count': tag_array['count'], 'link': tag_array['tag']})
    context['tag_list'] = tag_list
    context['tag'] = tag
    if size == 'qhd':
        width_min = 2560
        height_min = 1440
    elif size == 'fhd':
        width_min = 1920
        height_min = 1080
    else:
        width_min = 1280
        height_min = 720
    img_list = Images.objects.filter(rating=rate).filter(tags__contains=tag).filter(
        width__gte=width_min).filter(height__gte=height_min).filter(favor__gte=0).order_by('-serial')
    paginator = Paginator(img_list, 20)  # Show 20 contacts per page
    try:
        images = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        images = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        images = paginator.page(paginator.num_pages)
    context['images'] = images
    return render(request, 'index.html', context)

